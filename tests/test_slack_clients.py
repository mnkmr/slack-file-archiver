import os
import pytest
import tempfile
from slack_file_archiver import slack_clients


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_slack_client_list_files():
    slack = slack_clients.slack_client(os.environ["SLACK_API_TOKEN"])
    files = slack.list_files()
    assert isinstance(files, list)
    assert len(files) > 0


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_slack_client_download():
    slack = slack_clients.slack_client(os.environ["SLACK_API_TOKEN"])
    with tempfile.TemporaryDirectory() as p:
        files = slack.list_files(ts_from=0.0, ts_to=1652886000.0)
        file = files[0]
        filepath = os.path.join(p, file["title"])
        slack.download(file, filepath)
        assert os.path.isfile(filepath)
        assert os.path.getsize(filepath) > 0


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_slack_client_channel_name():
    slack = slack_clients.slack_client(os.environ["SLACK_API_TOKEN"])

    ch_id = "C03FH6VNL4S"
    name = slack.channel_name(ch_id)
    assert name == "general"

    ch_id = "C03FH6QD01H"
    name = slack.channel_name(ch_id)
    assert name == "random"


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_slack_client_group_name():
    slack = slack_clients.slack_client(os.environ["SLACK_API_TOKEN"])

    ch_id = "C03GCT2NGLV"
    name = slack.group_name(ch_id)
    assert name == "test-private"


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_slack_client_im_name():
    slack = slack_clients.slack_client(os.environ["SLACK_API_TOKEN"])

    ch_id = "D03FH7RR2KD"
    name = slack.im_name(ch_id)
    assert name == "slack-file-archiver"
