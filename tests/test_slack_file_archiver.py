import glob
import os
import pytest
import tempfile
import slack_file_archiver as fa


def test_opt_dest():
    if "SLACK_FILE_ARCHIVER_DESTINATION" in os.environ:
        del os.environ
    expect = os.path.join(os.getcwd(), "slack_files_archived")
    assert fa.opt_dest(None) == expect

    with tempfile.TemporaryDirectory() as a:
        with tempfile.TemporaryDirectory() as b:
            os.environ["SLACK_FILE_ARCHIVER_DESTINATION"] = a
            assert fa.opt_dest(None) == a
            assert fa.opt_dest(b) == b
            not_exist_a = a
            not_exist_b = b

    os.environ["SLACK_FILE_ARCHIVER_DESTINATION"] = not_exist_a
    with pytest.raises(FileNotFoundError):
        fa.opt_dest(None)

    with pytest.raises(FileNotFoundError):
        fa.opt_dest(b)


def test_last_timestamp():
    with tempfile.TemporaryDirectory() as p:
        ts = fa.last_timestamp(p, fa.LAST_TIMESTAMP_FILES)
        assert ts == None

    with tempfile.TemporaryDirectory() as p:
        dllog = os.path.join(p, fa.LAST_TIMESTAMP_FILES)
        with open(dllog, "w") as f:
            f.write("1.0\n")
        ts = fa.last_timestamp(p, fa.LAST_TIMESTAMP_FILES)
        assert ts == 1.0


def test_opt_ts_from():
    ts_from = fa.opt_ts_from(None, 1.0)
    assert ts_from == 1.0

    ts_from = fa.opt_ts_from(0.0, 1.0)
    assert ts_from == 0.0

    with pytest.raises(ValueError):
        fa.opt_ts_from(None, -1.0)

    with pytest.raises(ValueError):
        fa.opt_ts_from(-1.0, 0.0)


def test_opt_ts_to():
    ts_to = fa.opt_ts_to(None)
    assert ts_to is None

    ts_to = fa.opt_ts_to(0.0)
    assert ts_to == 0.0

    with pytest.raises(ValueError):
        fa.opt_ts_to(-1.0)


@pytest.mark.skipif("SLACK_API_TOKEN" not in os.environ,
                    reason="SLACK_API_TOKEN is not set")
def test_archive():
    token = os.environ["SLACK_API_TOKEN"]
    with tempfile.TemporaryDirectory() as p:
        timestamp_file = "LAST_TIMESTAMP_FILES"
        fa.files.archive(token, p, timestamp_file, ts_to=1652886000.0)
        yeardir = os.path.join(p, "2022")
        assert os.path.isdir(yeardir)

        files = glob.glob(os.path.join(yeardir, "channel-general", "*"))
        assert len(files) == 2
        assert os.path.getsize(files[0]) > 0
        assert os.path.getsize(files[1]) > 0
