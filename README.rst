Slack-file-archiver
###################

What for?
=========
This is a slack app to download files on slack. This app can download the files from the channels which the app account is joinned.

Install to a workspace
======================
#. Access https://api.slack.com/ and click Your apps (You may need login)
#. Click **Create New App** -> **From scratch**
#. Enter app name (for example "file-archiver") and Choose a workspace to add the app
#. Click **Create App**
#. Click **OAuth&Permissions** in the right menu column, then Scopes -> Bot Token Scopes (or User Token Scopes) -> **Add an OAuth Scope**
#. Add **files:read**, **users:read**, **channels:read**, **channels:history**, **groups:read**, **groups:history**, **im:read**, **im:history**,  **mpim:read**, and **mpim:history**
#. In OAuth&Permissions menu, OAuth Tokes for Your Workspace -> **Install to Workspace**
#. Check the displayed OAuth Token

Use Bot Token to download files in the channels that the slack-file-archiver has been added to.
Use User Token to download files in all public channels and the slack-file-archiver has been added to.

How to use
==========

::

    # Download all the files in this repository
    git clone https://gitlab.com/mnkmr/slack-file-archiver

    cd slack-file-archiver

    # Install this package
    python -m pip install .
    # Or just for only one-time use (https://pip.pypa.io/en/stable/topics/local-project-installs/?highlight=local#editable-installs)
    # python -m pip install -e .

    # Install requirements
    # You need to install poetry (pip install poetry) in advance
    poetry install --no-dev

    # Set OAuth Token into an environmental variable "SLACK_API_TOKEN"
    # cmd.exe: set SLACK_API_TOKEN=xoxp-0000000000000-0000000000000-0000000000000-00000000000000000000000000000000
    # powershell: $Env:SLACK_API_TOKEN="xoxp-0000000000000-0000000000000-0000000000000-00000000000000000000000000000000"
    # This is for Unix-like system.
    export SLACK_API_TOKEN="xoxp-0000000000000-0000000000000-0000000000000-00000000000000000000000000000000"

    poetry shell
    python -m slack_file_archiver

Command-line options
===================

* :code:`--dest`
  Path to save downloaded files. An environmental variable
  :code:`SLACK_FILE_ARCHIVER_DESTINATION` is reffered if this option is not
  assigned. "slack_files_archived" directory will be made under the current
  working directory to save downloaded files if both of those are not
  specified.

* :code:`--ts-from`
  The beginning date to retrieve in Unix timestamp.

* :code:`--ts-to`
  The final date to retrieve in Unix timestamp.

Usage examples
==============

::

  >> python -m slack_file_archiver

  >> python -m slack_file_archiver --dest=C:\Users\myname\slack_files

  >> python -m slack_file_archiver --ts-from=1640962800 --ts-to=1648652400


Known issue
===========
* The command-line options are not user-friendly. I will add another options for human if someone wants.
