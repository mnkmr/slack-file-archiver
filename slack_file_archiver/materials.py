import os
import re
import shutil


def copy(archived: list, identifier: str, destdir: str):
    # check the destdir directory
    if not os.path.isdir(destdir):
        os.makedirs(destdir, exist_ok=True)

    year_pat = "[0-9０-９]{4}"
    month_pat = "([0０][1-9１-９]|[1１][0-2０-２])"
    day_pat = "([0０][1-9１-９]|[12１２][0-9０-９]|[3３][01０１])"
    filename_pattern = "^" + year_pat + month_pat + day_pat + identifier + "_"
    filename_re = re.compile(filename_pattern, re.I)
    for filepath, user_name, file in archived:
        if not re.match(filename_re, file["title"]):
            continue
        userdir = os.path.join(destdir, user_name)
        os.makedirs(userdir, exist_ok=True)
        destpath = os.path.join(userdir, file["title"])
        shutil.copyfile(filepath, destpath)
