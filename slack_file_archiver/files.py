from datetime import datetime
from numbers import Number
import os
import traceback
from typing import Tuple

from . import slack_clients
from . import loggers


def archive(token: str, archive_path: str, timestamp_file: str, **kwargs) -> list:
    """Download files uploaded on slack."""
    slack = slack_clients.slack_client(token)
    log = loggers.logger(archive_path)
    IDs = log.downloaded_file_IDs()
    files = slack.list_files(**kwargs)
    # Remove file entries already downloaded
    filtered_files = filter(lambda file: file["id"] not in IDs, files)

    log.trim_downloadlog_file(log.LOG_TRIM_LEN)
    confirmed_paths = set()
    last_timestamp = -1.0
    archived = []
    try:
        for file in filtered_files:
            user_name = slack.user_real_name(file["user"])
            dt = datetime.fromtimestamp(file["created"])
            year = dt.strftime("%Y")
            ch_kind, ch_id = conversation_id(file)
            if not ch_id:
                continue
            if ch_kind == "channel":
                ch_name = slack.channel_name(ch_id)
            elif ch_kind == "group":
                ch_name = slack.group_name(ch_id)
            elif ch_kind == "im":
                ch_name = slack.im_name(ch_id)
            else:
                print(f"Unknown ch_kind: {ch_kind}")
                continue
            dirname = f"{ch_kind}-{ch_name}"
            destdir = os.path.join(archive_path, year, dirname)
            # Do not check the existence of the destdir even with exist_ok flag
            # to reduce disk access
            if destdir not in confirmed_paths:
                os.makedirs(destdir, exist_ok=True)
            confirmed_paths.add(destdir)
            prefix = dt.strftime("%Y%m%d%H%M%S_")
            filename = file["title"]
            filepath = os.path.join(destdir, prefix + filename)
            slack.download(file, filepath)
            log.download(file, filepath)
            archived.append((filepath, user_name, file))
            if file["created"] > last_timestamp:
                last_timestamp = file["created"]
    except Exception:
        tbstr = traceback.format_exc()
        log.error(tbstr)
        raise
    finally:
        slack.save_memo(archive_path)
        log.flush_all()
        timestamp_file = os.path.join(archive_path, timestamp_file)
        if isinstance(last_timestamp, Number) and last_timestamp >= 0:
            with open(timestamp_file, 'w', encoding="utf-8") as f:
                f.write(str(last_timestamp) + "\n")
        return archived


def conversation_id(file: dict) -> Tuple[str, str]:
    """Return a ID represent a (channel, group, im) posted"""
    if file["channels"]:
        return "channel", file["channels"][0]
    elif file["groups"]:
        return "group", file["groups"][0]
    elif file["ims"]:
        return "im", file["ims"][0]
    else:
        print(f"Unknown conversation: {file}")
        return "", ""
