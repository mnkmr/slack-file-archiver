from datetime import datetime
from glob import glob
import json
import os
import re
import traceback
from typing import Dict

from . import loggers
from . import slack_clients


def archive_messages(token: str, archive_path: str, msg_data_file: str, **kwargs):
    """Archive messages into files."""
    slack = slack_clients.slack_client(token)
    channels = slack.list_channels()
    log = loggers.logger(archive_path)
    data = []
    try:
        for ch in channels:
            data_add = get_messages(slack, ch, archive_path)
            data.extend(data_add)
        for ch, destdir, msg_list in data:
            os.makedirs(destdir, exist_ok=True)
            datafile = os.path.join(destdir, msg_data_file)
            if os.path.isfile(datafile):
                with open(datafile, mode='r', encoding="utf-8") as f:
                    old_list = json.load(f)
            else:
                old_list = []
            oldest_ts = msg_list[0]["ts"]
            old_list = [x for x in old_list if x["ts"] < oldest_ts]
            new_list = old_list + msg_list
            msg_list.clear()
            msg_list.extend(new_list)
            with open(datafile, mode='w', encoding="utf-8") as f:
                json.dump(msg_list, f)
    except Exception:
        tbstr = traceback.format_exc()
        log.error(tbstr)
        raise
    finally:
        # call for cache
        slack.list_users()
        slack.save_memo(archive_path)
    return data


def get_messages(slack, ch, archive_path, **kwargs):
    ch_kind = channel_kind(ch)
    ch_name = ch.get("name", "")
    dirname = f"{ch_kind}-{ch_name}"
    messages = slack.list_messages(ch["id"], **kwargs)
    messages.sort(key=lambda x: x["ts"])
    d = dict()
    for msg in messages:
        str_ts = msg["ts"]
        dt = datetime.fromtimestamp(float(str_ts))
        year = dt.strftime("%Y")
        destdir = os.path.join(archive_path, year, dirname)
        if destdir not in d:
            d[destdir] = []
        d[destdir].append(msg)
    message_data = []
    for destdir, msg_list in d.items():
        if msg_list:
            message_data.append((ch, destdir, msg_list))
    return message_data


def archive_replies(token: str, data: list, archive_path: str) -> None:
    slack = slack_clients.slack_client(token)
    log = loggers.logger(archive_path)
    confirmed_paths = set()
    try:
        for ch, destdir, msg_list in data:
            replydir = os.path.join(destdir, "REPLIES")
            if destdir not in confirmed_paths:
                os.makedirs(replydir, exist_ok=True)
                confirmed_paths.add(replydir)
            for msg in msg_list:
                if msg.get("reply_count", 0) < 1:
                    continue
                replies = slack.list_replies(ch["id"], msg["ts"])
                replyfile = os.path.join(replydir, msg["ts"])
                if os.path.isfile(replyfile):
                    with open(replyfile, mode='r', encoding="utf-8") as f:
                        old_list = json.load(f)
                else:
                    old_list = []
                ts_list = set([x["ts"] for x in replies])
                old_list = [x for x in old_list if x["ts"] not in ts_list]
                new_list = old_list + replies
                new_list.sort(key=lambda x: x["ts"])
                with open(replyfile, mode='w', encoding="utf-8") as f:
                    json.dump(new_list, f)
    except Exception:
        tbstr = traceback.format_exc()
        log.error(tbstr)
        raise


def save_human_readables(archive_path: str, msg_data_file: str, msg_txt_file: str):
    globpath = os.path.join(archive_path, "**", msg_data_file)
    data_paths = glob(globpath, recursive=True)
    for datafile in data_paths:
        save_human_readable(datafile, msg_txt_file)


def save_human_readable(datafile: str, msg_txt_file: str):
    destdir = os.path.dirname(datafile)
    archive_path = os.path.dirname(os.path.dirname(destdir))
    name_d = name_dict(archive_path)
    tp = text_prettifier(name_d)
    with open(datafile, 'r', encoding="utf-8") as f:
        data = json.load(f)
    lines = []
    for msg in data:
        str_ts = msg["ts"]
        dt = datetime.fromtimestamp(float(str_ts))
        date = dt.strftime("%Y/%m/%d %H:%M:%S")
        name = name_d.get(msg.get("user", ""), "Unknown")
        textlines = tp.prettify(msg["text"])
        attachedlines = attached_files(msg)
        replylines = replies(destdir, msg["ts"], name_d, tp)
        textlines += attachedlines + replylines
        textlines = ["\t" + line for line in textlines]
        textlines = [date + "," + name + ":"] + textlines
        text = "\n".join(textlines) + "\n"
        lines.append(text)
    if not lines:
        return
    textfile = os.path.join(destdir, msg_txt_file)
    with open(textfile, 'w', encoding="utf-8") as f:
        for linetext in lines:
            f.write(linetext)


def name_dict(archive_path):
    botname_file = os.path.join(archive_path, "display_name.json")
    with open(botname_file, 'r', encoding="utf-8") as f:
        botname_dict = json.load(f)
    # filter empty names
    botname_dict = {k: v for k, v in botname_dict.items() if v}

    realname_file = os.path.join(archive_path, "real_name.json")
    with open(realname_file, 'r', encoding="utf-8") as f:
        realname_dict = json.load(f)
    realname_dict = {k: v for k, v in realname_dict.items() if v}

    dispname_file = os.path.join(archive_path, "display_name.json")
    with open(dispname_file, 'r', encoding="utf-8") as f:
        dispname_dict = json.load(f)
    dispname_dict = {k: v for k, v in dispname_dict.items() if v}

    name_d = dict(dict(botname_dict, **realname_dict), **dispname_dict)
    return name_d


def channel_kind(ch) -> str:
    if ch.get("is_channel", False):
        ch_kind = "channel"
    elif ch.get("is_group", False) or ch.get("is_mpim", False):
        ch_kind = "group"
    elif ch.get("is_im", False):
        ch_kind = "im"
    else:
        ch_kind = ""
    return ch_kind


def attached_files(msg) -> list:
    if "files" not in msg:
        return []
    textlines = []
    textlines.append("添付ファイル" + "-"*66)
    for file in msg["files"]:
        if "name" in file:
            textlines.append(file["name"])
        elif file.get("mode", "") == "tombstone":
            textlines.append("このファイルは削除されました")
    textlines.append("-"*78)
    return textlines


def replies(destpath: str, ts: str, name_d: Dict[str, str], tp) -> list:
    replyfile = os.path.join(destpath, "REPLIES", ts)
    if not os.path.isfile(replyfile):
        return []
    with open(replyfile, 'r', encoding="utf-8") as f:
        replies = json.load(f)
    replylines = []
    replylines.append("返信" + "="*74)
    for msg in replies[1:]:
        str_ts = msg["ts"]
        dt = datetime.fromtimestamp(float(str_ts))
        date = dt.strftime("%Y/%m/%d %H:%M:%S")
        name = name_d.get(msg.get("user", ""), "Unknown")
        textlines = tp.prettify(msg["text"])
        attachedlines = attached_files(msg)
        textlines += attachedlines
        textlines = ["\t" + line for line in textlines]
        textlines = [date + "," + name + ":"] + textlines
        replylines.extend(textlines)
    replylines.append("="*78)
    return replylines


class text_prettifier:
    def __init__(self, name_d):
        self.p_gt = re.compile("&gt;")
        self.name_dict = name_d
        id_list = name_d.keys()
        self.p_user_id = re.compile("(?<=<@)(?:" + "|".join(id_list) + ")(?=>)")

    def prettify(self, text) -> str:
        textlines = text.splitlines()
        textlines = [self.substitute_user_id(line) for line in textlines]
        textlines = [self.p_gt.sub(">", line) for line in textlines]
        return textlines

    def substitute_user_id(self, text: str) -> str:
        m = self.p_user_id.search(text)
        if not m:
            return text
        match_set = set(self.p_user_id.findall(text))
        for usr_id in match_set:
            text = text.replace("<@" + usr_id + ">",
                                "@" + self.name_dict[usr_id])
        return text
