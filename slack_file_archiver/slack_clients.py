import copy
import json
import os
import requests
import sys
from slack_sdk import WebClient
from typing import Dict

# Max page number to load file information
PAGEMAX = 10000


class slack_client:
    """This class handles the communications with slack"""
    def __init__(self, token: str):
        self.download_headers = {"Authorization": "Bearer " + token}
        self.api = WebClient(token=token)
        self.memo: Dict[str, Dict[str, str]] = {}
        self.memo['channel']: Dict[str, str] = {}
        self.memo['display_name']: Dict[str, str] = {}
        self.memo['real_name']: Dict[str, str] = {}
        self.memo['bot']: Dict[str, str] = {}

    def load_memo(self, path: str) -> None:
        self._load_memo(path, "channel")
        self._load_memo(path, "display_name")
        self._load_memo(path, "real_name")
        self._load_memo(path, "bot")

    def save_memo(self, path: str) -> None:
        self._save_memo(path, "channel")
        self._save_memo(path, "display_name")
        self._save_memo(path, "real_name")
        self._save_memo(path, "bot")

    def _load_memo(self, path: str, kind: str) -> None:
        file = os.path.join(path, kind + ".json")
        if not os.path.isfile(file):
            return
        with open(file, 'r', encoding="utf-8") as f:
            d = json.load(f)
        for k, v in d.items():
            self.memo[kind][k] = v

    def _save_memo(self, path: str, kind: str) -> None:
        file = os.path.join(path, kind + ".json")
        fileexists = os.path.isfile(file)
        if fileexists:
            with open(file, 'r', encoding="utf-8") as f:
                d = json.load(f)
                self.memo[kind] = dict(d, **self.memo[kind])
        with open(file, 'w', encoding="utf-8") as f:
            json.dump(self.memo[kind], f, ensure_ascii=False)

    def list_files(self, **kwargs) -> list:
        """Return a list of file objects matched with the given criteria.

        Refer the official document for the details of file object.
        https://api.slack.com/types/file

        This is a thin wrapper of files_list() method of slack_sdk.Webclient
        class. This method automatically resolve paging and concatenate all the
        results acquired from the API. Therefore, the kwargs just follows the
        specs of the files_list() method.
        https://slack.dev/python-slack-sdk/api-docs/slack_sdk/web/client.html#slack_sdk.web.client.WebClient.files_list

        Returns
        -------
        files: list
            The list of file objects.
        """
        files = []
        for page in range(1, PAGEMAX + 1):
            res = self.api.files_list(count=100,
                                      **kwargs,
                                      page=page,
                                      show_files_hidden_by_limit=False)
            files.extend(res["files"])
            if res["paging"]["page"] >= res["paging"]["pages"]:
                break
        files.sort(key=lambda file: file["timestamp"])
        return files

    def list_channels(self) -> list:
        res = self.api.conversations_list()
        if not res.get("ok", False):
            return []
        channels = copy.copy(res["channels"])
        next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        while next_cursor:
            res = self.api.conversations_list(cursor=next_cursor)
            if not res.get("ok", False):
                break
            channels.append(res["channels"])
            next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        # Caching
        for ch in channels:
            if ch.get("is_channel", False):
                self.channel_name(ch["id"])
            elif ch.get("is_group", False):
                self.group_name(ch["id"])
            elif ch.get("is_im", False):
                self.im_name(ch["id"])
        return channels

    def list_users(self, **kwargs) -> list:
        res = self.api.users_list(**kwargs)
        if not res.get("ok", False):
            return []
        users = copy.copy(res["members"])
        next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        while next_cursor:
            res = self.api.users_list(**kwargs, cursor=next_cursor)
            if not res.get("ok", False):
                break
            users.extend(res["messages"])
            next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        # Caching
        for usr in users:
            self.user_name(usr["id"])
        return users

    def list_messages(self, channel: str, **kwargs) -> list:
        res = self.api.conversations_history(**kwargs, channel=channel)
        if not res.get("ok", False):
            return []
        messages = copy.copy(res["messages"])
        next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        while next_cursor:
            res = self.api.conversations_history(**kwargs, cursor=next_cursor, channel=channel)
            if not res.get("ok", False):
                break
            messages.extend(res["messages"])
            next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        return messages

    def list_replies(self, channel: str, ts: str, **kwargs) -> list:
        res = self.api.conversations_replies(**kwargs, channel=channel, ts=ts)
        if not res.get("ok", False):
            return []
        replies = copy.copy(res["messages"])
        next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        while next_cursor:
            res = self.api.conversations_replies(**kwargs, channel=channel,
                                                 ts=ts, cursor=next_cursor)
            if not res.get("ok", False):
                break
            replies.extend(res["messages"])
            next_cursor = res.get("response_metadata", {}).get("next_cursor", "")
        return replies

    def get_user_dict(self):
        users = self.list_users()
        user_dict: Dict[str, str] = {}
        for usr in users:
            usr_id = usr["id"]
            user_dict[usr_id] = self.user_name(usr_id)
        return user_dict

    def download(self, file: dict, filepath: str) -> None:
        """Retrieve the file data and save it to the assigned path.

        Parameters
        ----------
        file: dict
            A file objects returned by slack_sdk API.
            https://api.slack.com/types/file

        filepath: str
            A path to save the downloaded file including the filename.
        """
        url = file["url_private_download"]
        res = requests.get(url, headers=self.download_headers)
        with open(filepath, 'wb') as f:
            f.write(res.content)

    def channel_name(self, ch_id: str) -> str:
        """Return a channel title specified by a channel ID

        Parameters
        ----------
        ch_id: str
            A slack channel ID.

        Returns
        -------
        name: str
            Name of the public channel
        """
        # This function is memoized using self.memo
        if ch_id in self.memo["channel"]:
            return self.memo["channel"][ch_id]
        res = self.api.conversations_info(channel=ch_id)
        ch = res["channel"]
        # https://api.slack.com/types/channel
        name = ch["name"]
        self.memo["channel"][ch_id] = name
        return name

    def group_name(self, ch_id: str) -> str:
        """Return a private channel title specified by a channel ID

        Parameters
        ----------
        ch_id: str
            A slack channel ID.

        Returns
        -------
        name: str
            Name of the private channel
        """
        # This function is memoized using self.memo
        if ch_id in self.memo['channel']:
            return self.memo['channel'][ch_id]
        res = self.api.conversations_info(channel=ch_id)
        ch = res["channel"]
        # https://api.slack.com/types/group
        name = ch["name"]
        self.memo['channel'][ch_id] = name
        return name

    def im_name(self, ch_id: str) -> str:
        """Return a name of message channel specified by a channel ID

        The name of im channel is the name of the other user.

        Parameters
        ----------
        ch_id: str
            A slack channel ID.

        Returns
        -------
        name: str
            Name of the user name of the direct message channel
        """
        # This function is memoized using self.memo
        if ch_id in self.memo['channel']:
            return self.memo['channel'][ch_id]
        res = self.api.conversations_info(channel=ch_id)
        ch = res["channel"]
        # https://api.slack.com/types/im
        name = self.user_real_name(ch["user"])
        self.memo['channel'][ch_id] = name
        return name

    def messaged_by(self, msg) -> str:
        user = msg.get("user", "")
        if user:
            return self.user_name(user)
        bot = msg.get("bot_id", "")
        if bot:
            return self.bot_name(bot)
        print(msg, file=sys.stderr)
        return "unknown"

    def user_name(self, usr_id: str) -> str:
        """Return a user name on slack.

        Parameters
        ----------
        usr_id: str
            A slack user ID.
        """
        name = self._user_name(usr_id, "display_name")
        if name:
            return name
        return self._user_name(usr_id, "real_name")

    def user_real_name(self, usr_id: str) -> str:
        return self._user_name(usr_id, "real_name")

    def user_display_name(self, usr_id: str) -> str:
        return self._user_name(usr_id, "display_name")

    def _user_name(self, usr_id: str, kind: str) -> str:
        """Return a real user name on slack.

        Parameters
        ----------
        usr_id: str
            A slack user ID.
        """
        # This function is memoized using self.memo
        if usr_id in self.memo[kind]:
            return self.memo[kind][usr_id]
        res = self.api.users_info(user=usr_id)
        name = res["user"]["profile"][kind]
        if name:
            self.memo[kind][usr_id] = name
        return name

    def bot_name(self, bot_id: str) -> str:
        # This function is memoized using self.memo
        if bot_id in self.memo["bot"]:
            return self.memo["bot"][bot_id]
        res = self.api.bots_info(bot=bot_id)
        bot_name = res["bot"]["name"]
        self.memo["bot"][bot_id] = bot_name
        return bot_name
