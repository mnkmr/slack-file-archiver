import csv
from datetime import datetime
import os
from typing import Any, Dict, Iterable, List, Optional, Tuple


class logger:
    def __init__(self, archive_path: str) -> None:
        trim_len_env = "SLACK_FILE_ARCHIVER_LOG_TRIM_LENGTH"
        self.LOG_TRIM_LEN = int(os.environ.get(trim_len_env, "10000"))
        self._BUFFER_LEN = 1000
        self._downloadlog_buffer: List[Tuple[Dict[Any, Any], str]] = []
        self.downloadlog_file = os.path.join(archive_path, "download.log")
        self.errorlog_file = os.path.join(archive_path, "error.log")

    def read_downloadlog_file(self) -> list:
        """Return the list of rows in the log file."""
        if not os.path.isfile(self.downloadlog_file):
            return []
        with open(self.downloadlog_file, 'r', encoding="utf-8") as f:
            reader = csv.reader(f, delimiter="\t")
            old_log = [row for row in reader]
        return old_log

    def trim_downloadlog_file(self, n: int) -> None:
        """Cut down the old lines in the log file"""
        old_log = self.read_downloadlog_file()
        if len(old_log) <= n:
            return
        rem = len(old_log) - n
        del old_log[:rem]
        with open(self.downloadlog_file, 'w', encoding="utf-8") as f:
            for row in old_log:
                f.write("\t".join(row))

    def download(self, file: dict, destpath: str) -> None:
        """Record a line of download log."""
        self._downloadlog_buffer.append((file, destpath))
        if len(self._downloadlog_buffer) > self._BUFFER_LEN:
            self.flush_downloadlog_buffer()

    def flush_downloadlog_buffer(self) -> None:
        """Write buffered download logs into the log file.

        Note that download logs are buffered.
        """
        if not self._downloadlog_buffer:
            return
        with open(self.downloadlog_file, 'a', encoding="utf-8") as f:
            for file, destpath in self._downloadlog_buffer:
                line = downloadlog_line(file, destpath)
                f.write(line)
        self._downloadlog_buffer.clear()

    def flush_all(self) -> None:
        """Write buffered all logs into the log file."""
        self.flush_downloadlog_buffer()

    def downloaded_file_IDs(self) -> list:
        """Return a set of file IDs already downloaded."""
        IDs = [file["id"] for file, _ in self._downloadlog_buffer]
        IDs.extend([row[2] for row in self.read_downloadlog_file()])
        downloaded = set(IDs)
        return IDs

    def error(self, tbstr: str) -> None:
        """Record a chunk of lines of an error raised."""
        logtime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        separator = "="*78
        with open(self.errorlog_file, 'a', encoding="utf-8") as f:
            f.write(f"\n{logtime}\n")
            f.write(tbstr)
            f.write(separator)


def downloadlog_line(file: dict, destpath: str) -> str:
    logtime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    timestamp = str(file["timestamp"])
    name = file["name"]
    ID = file["id"]
    size = str(file["size"])
    user = file["user"]
    channels = str(file["channels"])
    line = "\t".join([
        logtime,
        timestamp,
        ID,
        name,
        size,
        user,
        channels,
        destpath
        ]) + "\n"
    return line
