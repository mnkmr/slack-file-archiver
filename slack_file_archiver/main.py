import argparse
from numbers import Number
import os
from typing import Optional

from . import files
from . import materials
from . import messages


# The default directory name to be used if not specified
DEFAULT_ARCHIVE_DIRNAME = "slack_files_archived"

# The default directory name to be used if not specified
DEFAULT_MATERIALS_DIRNAME = "materials_archived"

# The file name to save the last timestamp to archive files
LAST_TIMESTAMP_FILES = "LAST_TIMESTAMP_FILES"

# The file and directory names to store messages
MESSAGE_DATA_FILENAME = "MESSAGEDATA.JSON"
MESSAGE_TEXT_FILENAME = "MESSAGE.TXT"


def main() -> None:
    if "SLACK_API_TOKEN" not in os.environ:
        raise RuntimeError(
                "an environment variable SLACK_API_TOKEN should be set.")
    token = os.environ["SLACK_API_TOKEN"]

    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--dest",
            type=str,
            help="Path to save downloaded files.")
    parser.add_argument(
            "--materials-dest",
            type=str,
            help="Path to copy downloaded material files.")
    parser.add_argument(
            "--ts-from",
            type=float,
            help="Unix timestamp of the beginning date to retrieve")
    parser.add_argument(
            "--ts-to",
            type=float,
            help="Unix timestamp of the final date to retrieve")
    parser.add_argument(
            "--no-file",
            action="store_true",
            help="Do not archive files")
    parser.add_argument(
            "--no-message",
            action="store_true",
            help="Do not archive messages")
    args = parser.parse_args()
    archive_path = opt_dest(args.dest)
    materials_path = opt_materials_dest(args.materials_dest)
    if not args.no_file:
        ts_from_file_default = last_timestamp(archive_path, LAST_TIMESTAMP_FILES)
        ts_from_file = opt_ts_from(args.ts_from, ts_from_file_default)
        ts_to = opt_ts_to(args.ts_to)
        archived = files.archive(token, archive_path, LAST_TIMESTAMP_FILES,
                                 ts_from=ts_from_file, ts_to=ts_to)

        seminar_dest = os.path.join(materials_path, "seminar")
        materials.copy(archived, "seminar", seminar_dest)
        group_meeting_dest = os.path.join(materials_path, "group_meeting")
        materials.copy(archived, "gm", group_meeting_dest)

    if not args.no_message:
        msg_data_file = MESSAGE_DATA_FILENAME
        msg_txt_file = MESSAGE_TEXT_FILENAME
        data = messages.archive_messages(token, archive_path, msg_data_file)
        messages.archive_replies(token, data, archive_path)
        messages.save_human_readables(archive_path, msg_data_file, msg_txt_file)


def opt_dest(arg: Optional[str]) -> str:
    check_existence = True
    if arg is not None:
        archive_path = arg
    elif "SLACK_FILE_ARCHIVER_DESTINATION" in os.environ:
        archive_path = os.environ["SLACK_FILE_ARCHIVER_DESTINATION"]
    else:
        cwd = os.getcwd()
        archive_path = os.path.join(cwd, DEFAULT_ARCHIVE_DIRNAME)
        check_existence = False
    assert isinstance(archive_path, str)
    if check_existence and not os.path.isdir(archive_path):
        raise FileNotFoundError(f"No such directory: {archive_path}")
    return archive_path


def opt_materials_dest(arg: Optional[str]) -> str:
    check_existence = True
    if arg is not None:
        dest_path = arg
    elif "SLACK_FILE_ARCHIVER_MATERIALS_DESTINATION" in os.environ:
        dest_path = os.environ["SLACK_FILE_ARCHIVER_MATERIALS_DESTINATION"]
    else:
        cwd = os.getcwd()
        dest_path = os.path.join(cwd, DEFAULT_MATERIALS_DIRNAME)
        check_existence = False
    assert isinstance(dest_path, str)
    if check_existence and not os.path.isdir(dest_path):
        raise FileNotFoundError(f"No such directory: {dest_path}")
    return dest_path


def last_timestamp(archive_path: str, filename: str) -> Optional[float]:
    timestamp_file = os.path.join(archive_path, filename)
    if not os.path.isfile(timestamp_file):
        return None
    with open(timestamp_file, 'r', encoding="utf-8") as f:
        ts = float(f.read())
    return ts


def opt_ts_from(arg: Optional[float], ts_from_default: Optional[float]) -> Optional[float]:
    if arg is None:
        ts_from = ts_from_default
    else:
        ts_from = float(arg)
    if isinstance(ts_from, Number):
        if ts_from < 0:
            raise ValueError(
                    "Time stamp of the beginning date should be 0 or larger.")
    return ts_from


def opt_ts_to(arg: Optional[float]) -> Optional[float]:
    if arg is None:
        return arg
    ts_to = float(arg)
    if isinstance(arg, Number):
        if ts_to < 0:
            raise ValueError(
                    "Time stamp of the final date should be 0 or larger.")
    return ts_to
